Name:             freeipmi
Version:          1.6.14
Release:          1
Summary:          IPMI remote console and system management software
License:          GPLv3+
URL:              http://www.gnu.org/software/freeipmi/
Source0:          http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
Source1:          bmc-watchdog.service
Source2:          ipmidetectd.service
Source3:          ipmiseld.service

BuildRequires:    libgcrypt-devel texinfo systemd gcc
%{?systemd_requires}

Requires(post):   ldconfig
Provides:         %{name}-bmc-watchdog = %{version}-%{release} %{name}-ipmidetectd = %{version}-%{release} %{name}-ipmiseld = %{version}-%{release}
Obsoletes:        %{name}-bmc-watchdog < %{version}-%{release} %{name}-ipmidetectd < %{version}-%{release} %{name}-ipmiseld < %{version}-%{release}

%description
The package provides "Remote-Console" and
"System Management software" based on intelligent
platform management interface specification.

%package          devel
Summary:          Development package for Freeipmi
Requires:         %{name} = %{version}-%{release}

%description      devel
Development package for Freeipmi. This package includes the Freeipmi
header files and libraries.

%package          help
Summary:          help for freeipmi
Requires:         %{name} = %{version}-%{release}

%description help
The help package contains manual pages and other related files for freeipmi.

%if %{?_with_debug:1}%{!?_with_debug:0}
  %global _enable_debug --enable-debug --enable-trace --enable-syslog
%endif

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="-D_GNU_SOURCE $RPM_OPT_FLAGS"
%configure --program-prefix=%{?_program_prefix:%{_program_prefix}} %{?_enable_debug} --disable-static
%make_build

%install
%make_install
echo freeipmi > %{buildroot}%{_localstatedir}/lib/freeipmi/ipckey

%delete_la

install -m755 -d %{buildroot}%{_unitdir}
install -pm644 %SOURCE1 %SOURCE2 %SOURCE3 %{buildroot}%{_unitdir}
rm -frv %{buildroot}%{_initrddir} %{buildroot}%{_sysconfdir}/init.d

%preun
if [ $1 = 0 ]; then
    install-info --delete %{_infodir}/freeipmi-faq.info.gz %{_infodir}/dir &>/dev/null || :
fi
%systemd_preun bmc-watchdog.service
%systemd_preun ipmiseld.service
%systemd_preun ipmidetectd.service

%post
install-info %{_infodir}/freeipmi-faq.info.gz %{_infodir}/dir &>/dev/null || :
%systemd_post bmc-watchdog.service
%systemd_post ipmiseld.service
%systemd_post ipmidetectd.service
/sbin/ldconfig

%postun
/sbin/ldconfig
%systemd_postun_with_restart bmc-watchdog.service
%systemd_postun_with_restart ipmiseld.service
%systemd_postun_with_restart ipmidetectd.service

%files
%dir %{_sysconfdir}/freeipmi/
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/freeipmi/*.conf
%{_libdir}/libipmiconsole*so.*
%doc %{_datadir}/doc/%{name}/AUTHORS
%doc %{_datadir}/doc/%{name}/COPYING
%doc %{_datadir}/doc/%{name}/ChangeLog
%doc %{_datadir}/doc/%{name}/ChangeLog.0
%doc %{_datadir}/doc/%{name}/INSTALL
%doc %{_datadir}/doc/%{name}/NEWS
%doc %{_datadir}/doc/%{name}/README
%doc %{_datadir}/doc/%{name}/README.argp
%doc %{_datadir}/doc/%{name}/README.build
%doc %{_datadir}/doc/%{name}/README.openipmi
%doc %{_datadir}/doc/%{name}/TODO
%doc %{_datadir}/doc/%{name}/COPYING.*
%{_libdir}/libfreeipmi*so.*
%{_libdir}/libipmidetect*so.*
%{_libdir}/libipmimonitoring.so.*
%{_localstatedir}/lib/*
%{_sbindir}/*
%config(noreplace) %{_sysconfdir}/sysconfig/bmc-watchdog
%{_unitdir}/bmc-watchdog.service
%{_unitdir}/ipmidetectd.service
%{_unitdir}/ipmiseld.service
%dir %{_localstatedir}/cache/ipmimonitoringsdrcache
%dir %{_localstatedir}/cache/ipmiseld

%files devel
%dir %{_datadir}/doc/%{name}/contrib/libipmimonitoring
%doc %{_datadir}/doc/%{name}/contrib/libipmimonitoring/*
%{_libdir}/libipmiconsole.so
%{_libdir}/libfreeipmi.so
%{_libdir}/libipmidetect.so
%{_libdir}/libipmimonitoring.so
%dir %{_includedir}/freeipmi
%{_includedir}/*
%{_libdir}/pkgconfig/*

%files help
%doc %{_datadir}/doc/%{name}/DISCLAIMER.*
%doc %{_datadir}/doc/%{name}/freeipmi-*
%dir %{_datadir}/doc/%{name}
%dir %{_datadir}/doc/%{name}/contrib
%dir %{_datadir}/doc/%{name}/contrib/ganglia
%doc %{_datadir}/doc/%{name}/contrib/ganglia/*
%dir %{_datadir}/doc/%{name}/contrib/nagios
%doc %{_datadir}/doc/%{name}/contrib/nagios/*
%dir %{_datadir}/doc/%{name}/contrib/pet
%doc %{_datadir}/doc/%{name}/contrib/pet/*
%doc %{_infodir}/*
%{_mandir}/man8/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%{_mandir}/man3/*
%exclude %{_infodir}/dir

%changelog
* Mon Mar 04 2024 xuhe <xuhe@kylinos.cn> - 1.6.14-1
- Upgrade version to 1.6.14

* Fri Jan 26 2024 yangjunshuo <yangjunshuo@huawei.com> - 1.6.11-1
- Upgrade version to 1.6.11

* Fri Oct 13 2023 xu_ping <707078654@qq.com> - 1.6.10-1
- Upgrade version to 1.6.10

* Wed Mar 22 2023 Ge Wang <wangge20@h-partners.com> - 1.6.8-4
- fix illegal memory access

* Thu Jan 05 2023 chenmaodong <chenmaodong@xfusion.com> - 1.6.8-3
- fix invalid template check error

* Thu Dec 29 2022 chenmaodong <chenmaodong@xfusion.com> - 1.6.8-2
- ipmi-config: fix output corner case due to missing error handling

* Fri Jan 14 2022 caodongxia <caodongxia@huawei.com> - 1.6.8-1
- Upgrade 1.6.8

* Wed Jun 2 2021 baizhonggui <baizhonggui@huawei.com> - 1.6.2-5
- Fix building error: configure: error: no acceptable C compiler found in $PATH
- Add gcc in BuildRequires

* Sat Nov 30 2019 wangye<wangye54@huawei.com> - 1.6.2-4
- Package init

* Thu Nov 21 2019 wangye<wangye54@huawei.com> - 1.6.2-3
- Package init
